const possibleValues = ['BLOCK', 'SHOOT', 'EVADE', 'RELOAD']

var opponent = {
	bullets: 0,
	blocks: 0,
	evade: 0
};

var doomGuy = {
	bullets: 0,
	blocks: 0,
	evade: 0,
	shootingSequence: 0,
	blockingSequence: 0
}

makeMove = (lastMove) => {
	console.log('Opponents last move was: ');
	console.log(lastMove);

	var move;

	if (lastMove.sequenceNumber === 0) {
		opponent.blocks = 0;
		opponent.bullets = 0;
		opponent.evade = 0;
		doomGuy.blocks = 0;
		doomGuy.bullets = 0;
		doomGuy.evade = 0;
		doomGuy.shootingSequence = 0;
	}

	if (lastMove.opponentsLastMove === "BLOCK") {
		if (opponent.blocks < 4) {
			opponent.blocks = opponent.blocks + 1;
		}
	}
	if (lastMove.opponentsLastMove === "SHOOT") {
		if (opponent.bullets > 0) {
			opponent.bullets = opponent.bullets - 1;
		}
		opponent.blocks = 0;
	}
	if (lastMove.opponentsLastMove === "RELOAD") {
		if (opponent.bullets < 6) {
			opponent.bullets = opponent.bullets + 1;
		}
		opponent.blocks = 0;
	}
	if (lastMove.opponentsLastMove === "EVADE") {
		if (opponent.evade === 0) {
			opponent.evade = 1;
		}
		opponent.blocks = 0;
	}

	if (lastMove.sequenceNumber === 0) {
		move = "RELOAD";
	} else if (doomGuy.shootingSequence) {
		move = "SHOOT";
	} else if (50 - lastMove.sequenceNumber <= doomGuy.bullets) {
		move = "SHOOT";
	} else if (opponent.bullets === 0) {
		move = "RELOAD";
		if (doomGuy.bullets > 1) {
			doomGuy.shootingSequence = 1;
		}
	} else if (opponent.bullets > 4 && doomGuy.evade === 0 && !doomGuy.blockingSequence) {
		doomGuy.blockingSequence = 1;
		if (doomGuy.blocks > 3) {
			move = "EVADE"
		} else {
			move = "BLOCK";
		}
	} else if (doomGuy.blockingSequence && doomGuy.blocks > 3 && doomGuy.evade === 1) {
		doomGuy.blockingSequence = 0;
		if (doomGuy.bullets > 0) {
			move = "SHOOT";
			doomGuy.shootingSequence = 1;
		} else {
			move = "RELOAD";
		}
	} else if (doomGuy.blockingSequence) {
		if (doomGuy.blocks > 3) {
			move = "EVADE"
		} else {
			move = "BLOCK";
		}
	} else if (doomGuy.bullets > 3) {
		move = "SHOOT";
		doomGuy.shootingSequence = 1;
	} else {
		doomGuy.blockingSequence = 0;
		doomGuy.shootingSequence = 0;
		var myMovesMaybe = ["RELOAD", "RELOAD"];
		if (doomGuy.blocks < 4) {
			myMovesMaybe.push("BLOCK");
			myMovesMaybe.push("BLOCK");
			myMovesMaybe.push("BLOCK");
		}
		if (doomGuy.bullets === 1) {
			myMovesMaybe.push("SHOOT");
		} else if (doomGuy.bullets === 2) {
			myMovesMaybe.push("SHOOT");
		} else if (doomGuy.bullets === 3) {
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
		} else if (doomGuy.bullets === 4) {
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
		} else if (doomGuy.bullets === 5) {
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
		} else if (doomGuy.bullets === 6) {
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
			myMovesMaybe.push("SHOOT");
		}
		move = myMovesMaybe[Math.floor(Math.random() * possibleValues.length)];
	}


	if (!move) {
		move = "RELOAD";
	}

	if (move === "SHOOT") {
		doomGuy.bullets = doomGuy.bullets - 1;
		doomGuy.blocks = 0;
		if (doomGuy.bullets === 0) doomGuy.shootingSequence = 0;
	} else if (move === "RELOAD") {
		doomGuy.bullets = doomGuy.bullets + 1;
		doomGuy.blocks = 0;
	} else if (move === "BLOCK") {
		doomGuy.blocks = doomGuy.block + 1;
	} else if (move === "EVADE") {
		doomGuy.evade = 1;
		doomGuy.blocks = 0;
	}

	console.log(`MY MOVE: ${move}; ROUND: ${lastMove.sequenceNumber}`);


	return { move: move }
};


module.exports = {
	makeMove: makeMove
};