const express = require('express')
const bodyParser = require('body-parser');
const player = require('./player');

const app = express();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/my-player/move', (req, res) => {
	let response = player.makeMove(req.body);
	res.send(response);
});

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`));