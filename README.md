# Dummy Java Player

## Start

```bash
npm install
```
```bash
npm start
```

Server is callable on http://localhost:8080/my-player/move
post example:

```json
{
    "sequenceNumber": 1,
    "opponentsLastMove": "RELOAD",
    "opponentId": "1234",
    "opponentName": "Muj tym"
}
```


## What it does

In `player.js` is basic logic
 * Log Input
 * Pick random value
 * Send it